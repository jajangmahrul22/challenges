<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Challenges-1</title>
  <style>
    button {
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
    }

    h2 {
      position: absolute;
      top: 40%;
      left: 50%;
      transform: translate(-50%, -50%);
    }
  </style>
</head>

<body>
  <h2 id="hello">Jajang</h2>
  <button onclick="hello()">Klik aku ^..^</button>
  <script>
    function hello() {
      var Button = document.getElementById("hello");
      Button.innerHTML = "Jajang Mahrul";
    }
  </script>
</body>

</html>